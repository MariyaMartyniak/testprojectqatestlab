package myprojects.automation.assignment2;

import myprojects.automation.assignment2.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.Map;

/**
 * Base script functionality, can be used for all Selenium scripts.
 */
public abstract class BaseScript {
    private static ChromeDriver chromeDriver;

    /**
     * @return New instance of {@link WebDriver} object.
     */


    // The method below is used to create WebDriver instance avoiding Chrome save password popup.
    // Additionally the window is being maximized
    public static WebDriver getDriver() throws Exception {

        if (chromeDriver == null) {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");
            Map prefs;
            prefs = new HashMap();
            prefs.put("credentials_enable_service", false);
            prefs.put("password_manager_enabled", false);
            options.setExperimentalOption("prefs", prefs);
            String driverPath = System.getProperty("user.dir") + "/driver/chromedriver.exe";
            if (driverPath == null)
                throw new Exception("Path to chrome driver is not specified");
            System.setProperty("webdriver.chrome.driver", driverPath);
            chromeDriver = new ChromeDriver(options);
        }
        return chromeDriver;
    }


    public static void loginPrestashopAdmin() {
        try {
            WebDriver driver = getDriver();
            driver.get(Properties.getBaseAdminUrl());
            WebElement email = driver.findElement(By.id("email"));
            email.sendKeys("webinar.test@gmail.com");

            WebElement passwd = driver.findElement(By.name("passwd"));
            passwd.sendKeys("Xcg7299bnSmMuRLp9ITw");

            WebElement sbmtLogin = driver.findElement(By.name("submitLogin"));
            sbmtLogin.click();

            WebDriverWait wait = new WebDriverWait(driver, 10);

            WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.id("quick_select")));

        } catch (Exception e) {
            e.printStackTrace();


        }
    }
}
//     The method below is used to create WebDriver
//    public static WebDriver getDriver() throws Exception {
//        String driverPath = System.getProperty("user.dir") + "/driver/chromedriver.exe";
//        if (driverPath == null){
//            System.setProperty("webdriver.chrome.driver", driverPath);
//        throw new Exception("Path to chrome driver is not specified");
//       }
//        return new ChromeDriver();
//    }
