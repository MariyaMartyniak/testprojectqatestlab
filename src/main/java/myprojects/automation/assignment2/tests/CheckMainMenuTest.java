package myprojects.automation.assignment2.tests;

import myprojects.automation.assignment2.BaseScript;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CheckMainMenuTest extends BaseScript {

    public static void main(String[] args) throws InterruptedException {

        try {
            WebDriver driver = getDriver();
            loginPrestashopAdmin();

            String xpath = "//*[contains(@class, 'maintab')]";
            String alterXpath = "//*[contains(@class, 'link-levelone')]";
            int elementsCount = driver.findElements(By.xpath(xpath)).size();
            for (int i = 1; i <= elementsCount; i++) {
                WebElement webElement =
                        driver.findElement(By.xpath((driver.findElements(By.xpath(xpath + "[" + i + "]")).size() > 0 ?
                                xpath : alterXpath) + "[" + i + "]"));
                webElement.click();
                Thread.sleep(1000);
                webElement = driver.findElement(By.xpath((driver.findElements(By.xpath(xpath + "[" + i + "]")).size() > 0 ?
                        xpath : alterXpath) + "[" + i + "]"));

                String expectedText = webElement.findElement(By.tagName("span")).getText();
                System.out.println(expectedText);
                driver.navigate().refresh();
                webElement = driver.findElement(By.xpath((driver.findElements(By.xpath(xpath + "[" + i + "]")).size() > 0 ?
                        xpath : alterXpath) + "[" + i + "]"));
              String actualText = webElement.findElement(By.xpath("//*[contains(@class, 'maintab') and contains(@class, 'active')  or contains(@class, 'link-levelone') and contains(@class, '-active')  ]") ).findElement(By.tagName("span")).getText();

                Assert.assertEquals(expectedText,actualText);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
