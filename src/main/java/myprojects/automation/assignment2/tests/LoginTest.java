package myprojects.automation.assignment2.tests;

import myprojects.automation.assignment2.BaseScript;
import myprojects.automation.assignment2.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginTest extends BaseScript {

    public static void main(String[] args) throws InterruptedException {
        // TODO Script to execute login and logout steps
        try {
            WebDriver driver = getDriver();

            loginPrestashopAdmin();
            driver.findElement(By.id("employee_infos")).click();
            driver.findElement(By.id("header_logout")).click();

            driver.quit();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}

